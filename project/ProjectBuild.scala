import sbt._
import sbt.Keys._

object ProjectBuild extends Build {
  override lazy val settings = super.settings ++ Seq(
    name := "conversion-core",
    organization := "com.github.nyavro",
    version := "1.0-SNAPSHOT",
    scalaVersion := "2.11.4",
    autoCompilerPlugins := true,
    artifactName := { (sv: ScalaVersion, module: ModuleID, artifact: Artifact) =>
      artifact.name + "-" + module.revision + "." + artifact.extension
    },
    libraryDependencies ++= Seq(
      "org.scalatest" % "scalatest_2.11" % "2.2.3" % "test" withSources() withJavadoc(),
      "junit" % "junit" % "4.10" withSources() withJavadoc(),
      "org.specs2" % "specs2_2.11" % "2.3.12" withSources() withJavadoc(),
      "org.scalamock" %% "scalamock-scalatest-support" % "3.2.1" % "test"
    ),
    scalacOptions ++= Seq("-deprecation", "-unchecked"),
    publishMavenStyle := true,
    publishArtifact in Test := false,
    publishTo <<= version {
      v: String =>
        val nexus = "https://oss.sonatype.org/"
        if (v.trim.endsWith("SNAPSHOT"))
          Some("snapshots" at nexus + "content/repositories/snapshots")
        else
          Some("releases" at nexus + "service/local/staging/deploy/maven2")
    },
    pomExtra := (
      <url>https://github.com/nyavro/conversion-core</url>
        <licenses>
          <license>
            <name>APACHE-2.0</name>
            <url>http://www.apache.org/licenses/LICENSE-2.0</url>
            <distribution>repo</distribution>
          </license>
        </licenses>
        <scm>
          <url>git@github.com:nyavro/conversion-core.git</url>
          <connection>scm:git:git@github.com:nyavro/conversion-core.git</connection>
        </scm>
        <developers>
          <developer>
            <id>nyavro</id>
            <name>Evgeniy Nyavro</name>
            <url>https://github.com/nyavro</url>
          </developer>
        </developers>
      )

  )
}
