package com.nyavro.conversion.core.experimental

/**
 */
case class ConversionImpl[S, K, V, K1, V1, T](separation:Separation[S, (K, V)], transformation:Transformation[S, (K,V), (K1,V1), T], merge:Merge[(K1,V1),T]) extends Conversion[S, T]{

  def convert(source:S):T = merge.merge(separation.items(source).map(transformation.transform(this)))
}
