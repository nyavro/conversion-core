package com.nyavro.conversion.core.experimental

/**
 */
trait Conversion[S, T] {
  def convert(source:S):T
}
