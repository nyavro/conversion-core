package com.nyavro.conversion.core.experimental

/**
 */
trait Merge[P,T] {
  def merge(list: List[P]): T
}
