package com.nyavro.conversion.core.experimental

/**
 * Created by eny on 24.12.14.
 */
trait Transformation[S, I, O, T] {
  def transform(conversion: Conversion[S, T])(v:I):O
}
