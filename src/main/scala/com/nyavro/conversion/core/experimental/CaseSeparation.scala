package com.nyavro.conversion.core.experimental

import scala.reflect.ClassTag
import scala.reflect.runtime.universe._

/**
 * Case separation separates case class of type S to list of pairs of field name and value
 * @tparam S source instance type
 */
class CaseSeparation[S:ClassTag] extends Separation[S, (String, Any)] {
  override def items(source: S): List[(String, Any)] = {
    val mirror = runtimeMirror(getClass.getClassLoader).reflect(source)
    mirror
      .symbol
      .typeSignature
      .members
      .withFilter(_.isPrivate)
      .map {symbol => symbol.name.toString.trim -> mirror.reflectField(symbol.asTerm)}
      .map {case (key, value) => (key, value.bind(source).get)}
      .toList
  }
}
