package com.nyavro.conversion.core.experimental

import scala.reflect.ClassTag

/**
 */
class IdentityTransformation[S:ClassTag, T] extends
  Transformation[S, (String, Any), (String, Any), T] {

  val cls: Class[_] = implicitly[ClassTag[S]].runtimeClass
  
  override def transform(conversion: Conversion[S, T])(value: (String,Any)): (String, Any) = value match {
    case (k:String, v:Any) => (k, if (v.getClass==cls) conversion.convert(v.asInstanceOf[S]) else v)
  }
}
