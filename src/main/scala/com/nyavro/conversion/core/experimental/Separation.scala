package com.nyavro.conversion.core.experimental

/**
 * Separation splits source value of type S to list of pairs of type (K, V).
 * @tparam S source instance type
 * @tparam I target item type
 */
trait Separation[S, I] {

  /**
   * Returns list of pairs of separation.
   * @param source instance to split
   * @return list of pairs
   */
  def items(source:S):List[I]
}
