package com.nyavro.conversion.core.experimental

/**
 * Created by eny on 25.12.14.
 */
class MapMerge extends Merge[(String, Any),Map[String,Any]] {
  override def merge(list: List[(String, Any)]): Map[String, Any] = list.toMap
}
