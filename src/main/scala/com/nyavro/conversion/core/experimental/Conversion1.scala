package com.nyavro.conversion.core.experimental

import scala.reflect.ClassTag
import scala.reflect.runtime.universe._

/**
 */
class Conversion1[S:ClassTag]() {

  def convert(bean: S): Map[String, Any] = {
    val mirror = runtimeMirror(getClass.getClassLoader).reflect(bean)
    val mirrors =
      mirror
        .symbol
        .typeSignature
        .members
        .withFilter(_.isPrivate)
        .map {symbol => symbol.name.toString.trim -> mirror.reflectField(symbol.asTerm)}
        .toMap
    mirrors.map {case (key, value) => key -> value.bind(bean).get}
  }
}
