package com.nyavro.conversion.core.bean

import scala.reflect.ClassTag
import scala.reflect.runtime.universe._

/**
 * Extracts bean fields meta info for fast values extraction and delegates convert to underlying conversion
 * @author nyavro
 * @param provider
 * @tparam S source bean type
 * @tparam T target document type
 */

class BeanCachedConversion[S:ClassTag, T](provider:BeanTransformationFactory[S, T]) extends BeanConversion[S, T] {

  private var conversions = Map[Class[_], BeanConversion[S, T]]()

  def convert(doc:S) = {
    if (!conversions.contains(doc.getClass)) {
      val instanceMirror = runtimeMirror(getClass.getClassLoader).reflect(doc)
      conversions = conversions + (
        doc.getClass ->
          new BindingConversion(
            provider.transformation(this),
            instanceMirror
              .symbol
              .typeSignature
              .members
              .withFilter(_.isPrivate)
              .map { symbol => symbol.name.toString.trim -> instanceMirror.reflectField(symbol.asTerm)}
              .toMap
          )
        )
    }
    conversions(doc.getClass).convert(doc)
  }
}
