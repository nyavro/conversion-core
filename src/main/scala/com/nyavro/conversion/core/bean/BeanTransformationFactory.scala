package com.nyavro.conversion.core.bean

/**
 * Created by eny on 10/11/14.
 */
trait BeanTransformationFactory[S, T] {
  def transformation(conversion:BeanConversion[S, T]):BeanTransformation[S, T]
}
