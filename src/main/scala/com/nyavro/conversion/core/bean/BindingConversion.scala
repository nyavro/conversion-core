package com.nyavro.conversion.core.bean

import scala.reflect.runtime.universe._

/**
 * @author nyavro
 * BindingConversion
 */
class BindingConversion[S, T](mapping:BeanTransformation[S, T], mirrors:Map[String, FieldMirror]) extends BeanConversion[S, T] {
  def convert(doc:S):T = mapping.bind(
    mirrors.map {case (key, value) => mapping.translate(key) -> mapping.convert(value.bind(doc).get)}
  )
}
