package com.nyavro.conversion.core.bean

/**
 * Performs conversion one type to another. 
 * @tparam S source bean type
 * @tparam T target document type
 */
trait BeanConversion[S, T] {
  /**
   * Convert source bean value to target value
   * @param bean source to convert
   * @return
   */
  def convert(bean:S):T
}
