package com.nyavro.conversion.core.bean

/**
 * @author nyavro
 * Defines transformation from source type S (called 'bean') to target type T (called 'document')
 */
trait BeanTransformation[S,T] {

  /**
   * Converts individual component of the bean to some value of the target document
   * @param component individual value to convert
   * @return converted value
   */
  def convert(component:Any): S

  /**
   * Constructs target instance from collected map of transformed components
   * @param map of components converted method 'convert'
   * @return instance of target
   */
  def bind(map:Map[String,S]): T

  /**
   * Translates source key name to target key name
   * @param key source object component name
   * @return
   */
  def translate(key:String):String = key
}
