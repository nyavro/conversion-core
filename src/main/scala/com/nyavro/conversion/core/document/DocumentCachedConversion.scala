package com.nyavro.conversion.core.document

import scala.reflect.runtime.universe._

/**
 * @author nyavro
 * Extracts target type constructor and use it for conversions.
 * @param factory Document transformation factory to create new instances of converters
 * @tparam S source document type
 */
class DocumentCachedConversion[S](factory:DocumentTransformationFactory[S]) extends DocumentConversion[S] {
  
  private var conversions = Map[Type, DocumentConversion[S]]()

  override def convert[T:TypeTag](map:S) = convert(typeOf[T])(map)

  override def convert[T](typ:Type)(map:S) = {
    if(!conversions.contains(typ)) {
      conversions = conversions + (
        typ ->
          ConstructingConversion(
            factory.transformation(this),
            runtimeMirror(getClass.getClassLoader)
              .reflectClass(typ.typeSymbol.asClass)
              .reflectConstructor(typ.decl(termNames.CONSTRUCTOR).asMethod),
            typ
              .decl(termNames.CONSTRUCTOR)
              .asMethod
              .paramLists
              .headOption
          )
        )
    }
    conversions(typ).convert(map)
  }
}
