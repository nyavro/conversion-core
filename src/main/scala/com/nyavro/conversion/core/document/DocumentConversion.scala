package com.nyavro.conversion.core.document

import scala.reflect.runtime.universe._

/**
 * Defines conversion document of type S to target type T
 * @tparam S source document type
 */
trait DocumentConversion[S] {
  /**
   * Converts instance of given type S to target type T
   * @param doc source document to convert
   * @tparam T type of target object
   * @return Option of constructed type
   */
  def convert[T:TypeTag](doc:S):Option[T]

  /**
   * Converts document to given type
   * @param typ type of target object
   * @param doc instance of source type
   * @tparam T type of target object
   * @return Option of constructed type
   */
  def convert[T](typ:Type)(doc:S):Option[T]
}