package com.nyavro.conversion.core.document

/**
 * @author nyavro
 *         Date: 10.10.2014
 *         Time: 13:22
 */
object OptionUtil {
  implicit class Util[T](options: List[Option[T]]) {
    def sequence:Option[List[T]] =
      options.foldRight(Option(List[T]())) {
        (op, list) => (op, list) match {
          case (Some(x), Some(lst)) => Some(x :: lst)
          case _ => None
        }
      }
  }
}
