package com.nyavro.conversion.core.document

/**
 * Constructs instance of DocumentTransformation
 * @tparam S source instance type
 */
trait DocumentTransformationFactory[S] {
  def transformation(conversion:DocumentConversion[S]):DocumentTransformation[S]
}
