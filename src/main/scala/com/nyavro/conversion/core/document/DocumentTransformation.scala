package com.nyavro.conversion.core.document

import scala.reflect.runtime.universe._
/**
 * @author nyavro
 *         Date: 10.10.2014
 *         Time: 13:50
 */
trait DocumentTransformation[T] {
  def convert(symbol:Type, value:Any):Option[Any]
  def property(holder:T, name:String): Any
}
