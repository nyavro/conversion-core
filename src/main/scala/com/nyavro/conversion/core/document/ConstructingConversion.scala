package com.nyavro.conversion.core.document

import scala.reflect.runtime.universe._
import OptionUtil._

/**
 * Constructs instance of target type T from instance of source type S
 * @param transformation Upper level transformation
 * @param constructor MethodMirror representing constructor
 * @param parameters Constructor parameters
 * @tparam S source document type
 */
case class ConstructingConversion[S](transformation:DocumentTransformation[S], constructor: MethodMirror, parameters:Option[List[Symbol]]) extends DocumentConversion[S] {

  override def convert[T: TypeTag](source: S) =
    for (
      args <- parameters.map {list => list.map {arg => transformation.convert(arg.info, transformation.property(source, arg.name.toString))}};
      list <- args.sequence
    ) yield constructor(list: _*).asInstanceOf[T]

  override def convert[T](typ:Type)(map:S) = convert(map)
}
