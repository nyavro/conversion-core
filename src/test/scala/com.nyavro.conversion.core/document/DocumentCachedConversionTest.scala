package com.nyavro.conversion.core.document

import org.scalatest.{Matchers, FunSpec}

/**
 * @author nyavro
 *         Date: 08.10.2014
 *         Time: 16:49
 */
private case class Test2(a:Int, b:String)
private case class WrappingTest(v:Int, c:Test2)
class DocumentCachedConversionTest extends FunSpec with Matchers {
  describe("Converts values map to doc") {
    it("Creates case class") {
      new DocumentCachedConversion(PrimitiveDocumentProvider)
        .convert[Test2](Map("a" -> 11, "b" -> "2")) should === (Some(Test2(11, "2")))
    }
    it("Creates nested classes") {
      new DocumentCachedConversion(PrimitiveDocumentProvider)
        .convert[WrappingTest](Map("v" -> 121, "c" -> Map("a" -> 12, "b" -> "3"))) should === (Some(WrappingTest(121, Test2(12, "3"))))
    }
  }
}