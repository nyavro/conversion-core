package com.nyavro.conversion.core.document

import com.nyavro.conversion.core.bean.Test
import org.scalatest.{FunSpec, Matchers}

import scala.reflect.runtime.universe._

/**
 * @author nyavro
 *         Date: 08.10.2014
 *         Time: 16:49
 */
class ConstructingConversionTest extends FunSpec with Matchers {
  describe("Converts values map to doc") {
    it("Creates case class") {
      def conversion[T: TypeTag] =
        ConstructingConversion(
          new PrimitiveDocumentMapping(new DocumentCachedConversion(PrimitiveDocumentProvider)),
          runtimeMirror(getClass.getClassLoader)
            .reflectClass(typeOf[T].typeSymbol.asClass)
            .reflectConstructor(typeOf[T].decl(termNames.CONSTRUCTOR).asMethod),
          typeOf[T]
            .decl(termNames.CONSTRUCTOR)
            .asMethod
            .paramLists
            .headOption
        )
      conversion[Test].convert[Test](Map("a" -> 11, "b" -> "22")) should === (Some(Test(11, "22")))
    }
  }
  describe("Utils") {
    def listOptionsToOptionList[T](options: List[Option[T]]) =
      options.foldRight(Option(List[T]())) {
        (op, list) => (op, list) match {
          case (Some(x), Some(lst)) => Some(x::lst)
          case _ => None
        }
      }
    it("Converts list of options to option of list") {
      listOptionsToOptionList(List(Some("a"), Some("b"), None, Some("d"))) should === (None)
      listOptionsToOptionList(List(Some("a"), Some("b"), Some("c"))) should === (Some(List("a", "b", "c")))
    }
  }
}
