package com.nyavro.conversion.core.document


import scala.reflect.runtime.universe._

/**
 * @author nyavro
 *         Date: 10.10.2014
 *         Time: 13:52
 */
class PrimitiveDocumentMapping(conversion:DocumentConversion[Map[String,Any]]) extends DocumentTransformation[Map[String,Any]] {
  override def convert(typ: Type, value: Any) = value match {
    case i: Int => Some(i)
    case s: String => Some(s)
    case l: List[_] => Some(l)
    case m: Map[String,Any] => conversion.convert(typ)(m)
  }

  override def property(holder: Map[String, Any], name: String): Any = holder(name)
}

object PrimitiveDocumentProvider extends DocumentTransformationFactory[Map[String,Any]] {
  override def transformation(conversion: DocumentConversion[Map[String,Any]]) = new PrimitiveDocumentMapping(conversion)
}