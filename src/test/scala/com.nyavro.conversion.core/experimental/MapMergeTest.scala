package com.nyavro.conversion.core.experimental

import org.scalatest.{FunSpec, Matchers}

/**
 * Test case for MapMerge
 */
class MapMergeTest extends FunSpec with Matchers {
  describe("Map merge returns map") {
    it("Transforms input list of pairs to map") {
      new MapMerge().merge(List(("hello", "world"), ("test", 11), ("OK", 74))) should === (Map("hello"->"world", "test"->11, "OK"->74))
    }
  }
}
