package com.nyavro.conversion.core.experimental

import org.scalamock.scalatest.MockFactory
import org.scalatest.{FunSpec, Matchers}
/**
 * @author nyavro
 *         Date: 08.10.2014
 *         Time: 13:59
 */

case class SeparationTest(a:String)

class IdentityTransformationTest extends FunSpec with Matchers with MockFactory {

  describe("Converts values") {
    val conversion:Conversion[SeparationTest, String] = mock[Conversion[SeparationTest, String]]
    it("Converts int value as is") {
      new IdentityTransformation().transform(conversion)(("a", 137)) should === (("a", 137))
      new IdentityTransformation().transform(conversion)(("a", 731)) should === (("a", 731))
    }
    it("Converts double value as is") {
      new IdentityTransformation().transform(conversion)(("a", 13.7)) should === (("a", 13.7))
      new IdentityTransformation().transform(conversion)(("a", 7.31)) should === (("a", 7.31))
    }
    it("Converts string value as is") {
      new IdentityTransformation().transform(conversion)(("a", "scala")) should === (("a", "scala"))
      new IdentityTransformation().transform(conversion)(("a", "awesome")) should === (("a", "awesome"))
    }
    it("Converts map value as is") {
      new IdentityTransformation().transform(conversion)(("a", Map("key"->"val"))) should === (("a", Map("key"->"val")))
      new IdentityTransformation().transform(conversion)(("a", Map("first"->"second"))) should === (("a", Map("first"->"second")))
    }
    it("Converts list value as is") {
      new IdentityTransformation().transform(conversion)(("a", List("a","b"))) should === (("a", List("a","b")))
      new IdentityTransformation().transform(conversion)(("a", List("c","d"))) should === (("a", List("c","d")))
    }
  }
  describe("Delegates") {
    it("Transformation of composite instance to underlying conversion") {
      val conv:Conversion[SeparationTest, String] = stub[Conversion[SeparationTest, String]]
      new IdentityTransformation().transform(conv)(("a", SeparationTest("separation test")))
      (conv.convert _).verify(SeparationTest("separation test"))
    }
  }
}