package com.nyavro.conversion.core.experimental

import org.scalamock.scalatest.MockFactory
import org.scalatest.{FunSpec, Matchers}

case class Simple(a:Int, b:String)
case class Complex(c:Double, e:Simple)

/**
 * Test case for ConversionImpl.
 */
class ConversionImplTest extends FunSpec with Matchers with MockFactory {

  describe("Conversion full cycle") {
    val separation: Separation[Simple, (String, Any)] = new CaseSeparation[Simple]
    val transformation: Transformation[Simple, (String, Any), (String, Any), Map[String, Any]] = new IdentityTransformation[Simple, Map[String, Any]]
    it("Converts simple case class to map") {
      new ConversionImpl(separation, transformation, new MapMerge).convert(Simple(13, "thirteen")) should === (Map("a"->13, "b"->"thirteen"))
      new ConversionImpl(separation, transformation, new MapMerge).convert(Simple(14, "fourteen")) should === (Map("a"->14, "b"->"fourteen"))
    }
  }
}
