package com.nyavro.conversion.core.experimental

import org.scalatest.{FunSpec, Matchers}

/**
 * @author nyavro
 */

/**
 * Sample case class to split.
 * @param a Int field
 * @param b String field
 */
case class CSTest1(a:Int, b:String)

/**
 * Test case for CaseSeparation.
 */
class CaseSeparationTest extends FunSpec with Matchers {
  describe("Separates case class to fields") {
    it("Separates plain case class to map") {
      new CaseSeparation().items(CSTest1(17, "test")).toSet should === (Set(("a", 17), ("b", "test")))
      new CaseSeparation().items(CSTest1(19, "OK")).toSet should === (Set(("a", 19), ("b", "OK")))
    }
  }
}