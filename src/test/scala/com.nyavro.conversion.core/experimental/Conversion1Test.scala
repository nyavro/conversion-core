package com.nyavro.conversion.core.experimental

import org.scalatest.{FunSpec, Matchers}

case class Plain1(a:Int)
case class Plain2(a:Int, b:Int)

class Conversion1Test extends FunSpec with Matchers {
   describe("Converts bean to values map") {

     it("Extracts case class int field") {
       new Conversion1().convert(Plain1(11)) should === (Map("a"->11))
       new Conversion1().convert(Plain1(17)) should === (Map("a"->17))
     }

     it("Extracts case class int fields") {
       new Conversion1().convert(Plain2(13, 19)) should === (Map("a"->13, "b"->19))
       new Conversion1().convert(Plain2(21, 23)) should === (Map("a"->21, "b"->23))
     }
   }
 }