package com.nyavro.conversion.core.bean

import org.scalatest.{FunSpec, Matchers}

class PerformanceTest extends FunSpec with Matchers {
  describe("Performance") {
    it("Stress") {
      val conversion = new BeanCachedConversion(CaseToMapFactory)
      conversion.convert(SuperTest2("superTest_heating", Test(0, "a4"), 0, 3, "hhh", 1))
      def repeat(f: =>Unit)(n:Int):Unit = if(n>0) {
        f
        repeat(f)(n-1)
      } else {}
      val start = System.currentTimeMillis()
      var count = 0
      repeat( {
        conversion.convert(SuperTest2("superTest" + count, Test(count+3, "a4"+(count-1)), count, 3, "hhh", 1))
        //        should === (Map(
        //          "sa"->("superTest" + count),
        //          "sb"->Map("a"->(count+3), "b"->("a4" + (count-1)))))
        count = count + 1
      })(100000)
      println((System.currentTimeMillis() - start) + " " + count)
    }
  }
}