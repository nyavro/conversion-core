package com.nyavro.conversion.core.bean


/**
 * @author nyavro
 * Sample class converting case class fields to map of "field" -> value pairs
 */
class CaseToMap(conversion:BeanConversion[Any, Map[String, Any]]) extends BeanTransformation[Any, Map[String, Any]] {
  override def convert(primitive: Any): Any = primitive match {
    case i:Int => i
    case s:String => s
    case l:List[_] => l.map(convert)
    case m:Map[_,_] => m.map{case (k,v)=>(k, convert(v))}
    case _ => conversion.convert(primitive)
  }

  override def bind(map: Map[String, Any]): Map[String, Any] = map
}

object CaseToMapFactory extends BeanTransformationFactory[Any, Map[String, Any]] {
  override def transformation(conversion: BeanConversion[Any, Map[String, Any]]) = new CaseToMap(conversion)
}
