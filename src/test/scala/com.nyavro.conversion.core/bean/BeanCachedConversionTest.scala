package com.nyavro.conversion.core.bean

import org.scalatest.{FunSpec, Matchers}

case class ComplexList(a:String, b:List[Test])
case class ComplexMap(a:String, b:Map[String, Test])

class BeanCachedConversionTest extends FunSpec with Matchers {
  describe("Converts doc to values map") {
    it("Extracts case class fields") {
      val instance = Test(11, "74")
      new BeanCachedConversion(CaseToMapFactory).convert(instance) should === (Map("a"->11, "b"->"74"))
    }
    it("Extracts case class fields recursively") {
      val instance = SuperTest("super", Test(11,"74"))
      new BeanCachedConversion(CaseToMapFactory).convert(instance) should === (Map("sa"->"super", "sb"->Map("a"->11, "b"->"74")))
    }
    it("Processes list fields") {
      val instance = ListTest("listTest", List("a1", "b2"))
      new BeanCachedConversion(CaseToMapFactory).convert(instance) should === (Map("name"->"listTest", "list"->List("a1", "b2")))
    }
    it("Processes map fields") {
      val instance = MapTest("mapTest", Map("a3"->4, "a4"->5))
      new BeanCachedConversion(CaseToMapFactory).convert(instance) should === (Map("name"->"mapTest", "map"->Map("a3"->4, "a4"->5)))
    }
    it("Processes values in list") {
      val instance = ComplexList("hey", List(Test(1, "one"), Test(2, "two")))
      new BeanCachedConversion(CaseToMapFactory).convert(instance) should === (Map("a"->"hey", "b"->List(Map("a"->1, "b"->"one"), Map("a"->2, "b"->"two"))))
    }
    it("Processes values in map") {
      val instance = ComplexMap("hey", Map("key1"->Test(1, "one"), "key2"->Test(2, "two")))
      new BeanCachedConversion(CaseToMapFactory).convert(instance) should === (Map("a"->"hey", "b"->Map("key1"->Map("a"->1, "b"->"one"), "key2"->Map("a"->2, "b"->"two"))))
    }
  }
}